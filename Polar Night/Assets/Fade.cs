﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    [SerializeField] private float fadeInDelay = 3.0f; 
    [SerializeField] private Animator fadeAnimator;
    [SerializeField] private Animator fadeTextAnimator;
    
    void Start()
    {
        EventManager.StartListening(Constants.EventKashaReady, FadeOut);
        EventManager.StartListening(Constants.EventBoilerRefueled, FadeOut);
        
        EventManager.StartListening(Constants.EventCalledHome, FadeEnd);
    }

    private void FadeOut()
    {
        fadeAnimator.SetTrigger(Constants.AnimationTriggerFadeOut);
        Invoke(nameof(FadeIn), fadeInDelay);
    }

    private void FadeEnd()
    {
        fadeAnimator.SetTrigger(Constants.AnimationTriggerFadeOut);
        fadeTextAnimator.SetTrigger(Constants.AnimationTriggerFadeText);
    }

    private void FadeIn()
    {
        fadeAnimator.SetTrigger(Constants.AnimationTriggerFadeIn);
    }
}
