﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityBroken : MonoBehaviour
{
    [SerializeField] private List<Light> lights;

    private void Start()
    {
        EventManager.StartListening(Constants.EventGarlandPlaced, OnElectricityBroken);
        EventManager.StartListening(Constants.EventElectricityRepaired, OnElectricityRepaired);
    }

    private void OnElectricityBroken()
    {
        foreach (var light in lights)
        {
            light.enabled = false;
        }
    }

    private void OnElectricityRepaired()
    {
        foreach (var light in lights)
        {
            light.enabled = true;
        }
    }
}
