﻿using UnityEngine;

public class InteractableFurnace : InteractableBase, ITeleportable
{
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("BOILER REFUELED. WARM!");
        EventManager.TriggerEvent(Constants.EventBoilerRefueled);
        Destroy(gameObject);
    }

    private void Start()
    {
        EventManager.StartListening(Constants.EventBoilerTemperatureExamined, Activate);
    }
}
