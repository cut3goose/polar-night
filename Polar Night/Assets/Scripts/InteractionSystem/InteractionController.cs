﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    [Header("Data")] 
    [SerializeField] private InteractionInputData interactionInputData;
    [SerializeField] private InteractionData interactionData;

    [Space, Header("UI")] 
    [SerializeField] private InteractionUIPanel uiPanel;
    
    [Space, Header("Ray Settings")]
    [SerializeField] private float rayDistance;
    [SerializeField] float raySphereRadius;
    [SerializeField] LayerMask interactableLayer;
    
    private Camera _camera;
    private bool _interacting;
    private float _holdTimer;

    private void Awake()
    {
        _camera = FindObjectOfType<Camera>();
    }

    private void Start()
    {
        uiPanel.ResetUI();
    }

    private void Update()
    {
        CheckForInteractable();
        CheckForInteractableInput();
    }

    private void CheckForInteractable()
    {
        Ray _ray = new Ray(_camera.transform.position, _camera.transform.forward);
        RaycastHit _hitInfo;

        bool _hitSomething = Physics.SphereCast(_ray, raySphereRadius, out _hitInfo, rayDistance, interactableLayer);
        
        if (_hitSomething)
        {
            InteractableBase _interactable = _hitInfo.transform.GetComponent<InteractableBase>();

            if (_interactable != null && _interactable.GetType() != Memory.LastInteractedType)
            {
                if (interactionData.IsEmpty())
                {
                    interactionData.Interactable = _interactable;
                }
                else
                {
                    if (!interactionData.IsSameInteractable(_interactable))
                    {
                        interactionData.Interactable = _interactable;
                    }
                }
                UpdateTooltips();
            }
        }
        else
        {
            _interacting = false;
            uiPanel.ResetUI();
            interactionData.ResetData();
        }
        
        Debug.DrawRay(_ray.origin, _ray.direction * rayDistance, _hitSomething ? Color.green : Color.red);
    }

    private void CheckForInteractableInput()
    {
        if (interactionData.IsEmpty())
        {
            return;
        }

        if (interactionInputData.InteractedClicked)
        {
            _interacting = true;
            _holdTimer = 0.0f;
        }

        if (interactionInputData.InteractedRelease)
        {
            _interacting = false;
            _holdTimer = 0.0f;
            uiPanel.ResetUI();
            UpdateTooltips();
        }

        if (_interacting)
        {
            if (!interactionData.Interactable.IsInteractable)
            {
                return;
            }

            if (interactionData.Interactable.HoldInteract)
            {
                uiPanel.SetTooltip(interactionData.Interactable.TooltipHoldText);
                
                _holdTimer += Time.deltaTime;

                float heldPercent = _holdTimer / interactionData.Interactable.HoldDuration;
                uiPanel.UpdateProgressBar(heldPercent);

                if (heldPercent > 1.0f)
                {
                    interactionData.Interact();
                    _interacting = false;
                }
            }
            else
            {
                interactionData.Interact();
                _interacting = false;
            }
        }
    }

    private void UpdateTooltips()
    {
        if (interactionData.Interactable.IsInteractable)
        {
            uiPanel.SetTooltip(interactionData.Interactable.TooltipHoverText);
        }
        else
        {
            uiPanel.SetTooltip(interactionData.Interactable.TooltipNotInteractableText);
        }
    }
}
