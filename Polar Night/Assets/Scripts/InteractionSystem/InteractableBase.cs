﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class InteractableBase : MonoBehaviour
{
    protected AudioSource audioSource;

    [Header("Interactable Settings")] 
    [SerializeField] float holdDuration;

    [Space] 
    [SerializeField] private bool holdInteract;
    [SerializeField] private bool isInteractable;

    [Space] 
    [SerializeField] private string tooltipHoverText = Constants.TooltipHoverDefaultText;
    [SerializeField] private string tooltipHoldText = Constants.TooltipHoldDefaultText;
    [SerializeField] private string tooltipNotInteractableText = Constants.TooltipNotInteractableDefaultText;

    public float HoldDuration => holdDuration;
    public bool HoldInteract => holdInteract;
    public bool IsInteractable => isInteractable;

    public string TooltipHoverText
    {
        get => tooltipHoverText;
        set => tooltipHoverText = value;
    }

    public string TooltipHoldText
    {
        get => tooltipHoldText;
        set => tooltipHoldText = value;
    }

    public string TooltipNotInteractableText
    {
        get => tooltipNotInteractableText;
        set => tooltipNotInteractableText = value;
    }

    protected virtual void Start()
    {
        if (GetComponent<AudioSource>() != null)
        {
            audioSource = GetComponent<AudioSource>();
        }
    }

    public virtual void OnInteract()
    {
        if (audioSource?.clip != null)
        {
            SoundManager.singleton.PlayInteractingSound(audioSource);
        }

        Debug.Log("INTERACTED: " + gameObject.name);
    }

    public virtual void Activate()
    {
        isInteractable = true;
    }
    
    public void Deactivate(string newNonInteractableText = null)
    {
        isInteractable = false;
        if (newNonInteractableText != null)
        {
            tooltipNotInteractableText = newNonInteractableText;
            Debug.Log(tooltipNotInteractableText);
        }
    }
}
