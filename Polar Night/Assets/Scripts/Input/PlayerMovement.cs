﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement parameters")]
    [SerializeField] private float speed = 12.0f;
    [SerializeField] private float gravity = -9.81f;

    [Header("Ground check parameters")] 
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundDistance = 0.4f;
    private LayerMask groundMask;

    [Header("Animation Controllers")] 
    [SerializeField] private Animator leftHandController;
    [SerializeField] private Animator rightHandController;

    private CharacterController _characterController;
    private Vector3 _velocity;
    private bool _isGrounded;

    private void Start()
    {
        _characterController = GetComponent<CharacterController>();
        groundMask = LayerMask.GetMask(Constants.LayerBuilding, Constants.LayerSnow); 
        
        EventManager.StartListening(Constants.EventCalledHome, DisableControl);
    }

    private void DisableControl()
    {
        enabled = false;
    }

    // Update is called once per frame
    private void Update()
    {
        _isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (_isGrounded && _velocity.y < 0)
        {
            _velocity.y = -2.0f;
        }
        
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 moveDir = transform.right * x + transform.forward * z;

        bool isWalking = moveDir.magnitude > 1e-3f;
        leftHandController.SetBool(Constants.AnimationBoolWalking, isWalking);
        rightHandController.SetBool(Constants.AnimationBoolWalking, isWalking);

        _characterController.Move(moveDir * (speed * Time.deltaTime));

        _velocity.y += gravity * Time.deltaTime;
        _characterController.Move(_velocity * Time.deltaTime);
    }
}
