﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager singleton { get; private set; }

    [SerializeField] private GroundMaterialChecker groundMaterialChecker;

    [Header("Blizzard Volume")]
    [SerializeField] [Range(0f, 1f)] private float outdoorsVolume;
    [SerializeField] [Range(0f, 1f)] private float indoorsVolume;

    [Header("Sound Sources")]
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioSource footstepsSource;
    [SerializeField] private AudioSource blizzardSource;
    [SerializeField] private AudioSource ringSource;

    [Header("Sound Clips")]
    public AudioClip SnowWalkingSound;
    public AudioClip ConcreteWalkingSound;

    private bool _endGame;

    private void Awake()
    {
        singleton = this;
        blizzardSource.volume = indoorsVolume;
        _endGame = false;

        EventManager.StartListening(Constants.EventHouseEnter, OnHouseEnter);
        EventManager.StartListening(Constants.EventHouseExit, OnHouseExit);
        EventManager.StartListening(Constants.EventElectricityRepaired, EndGameSequence);
        EventManager.StartListening(Constants.EventCalledHome, DisableControl);
    }

    private void EndGameSequence()
    {
        _endGame = true;
    }
    
    private void DisableControl()
    {
        enabled = false;
    }

    private void Update()
    {
        TryPlayWalkingSound();
    }

    public void TryPlayWalkingSound()
    {
        if (Input.GetKey(KeyCode.W) && footstepsSource.isPlaying == false)
        {
            footstepsSource.clip = groundMaterialChecker.GetFootstepSound();
            footstepsSource.Play();
        }
    }

    public void PlayInteractingSound(AudioSource audioSource)
    {
        if (audioSource.isPlaying == false)
        {
            audioSource.Play();
        }
    }

    private void OnHouseEnter()
    {
        blizzardSource.volume = indoorsVolume;
        if (_endGame)
        {
            if (musicSource && musicSource.isPlaying)
            {
                musicSource.Stop();
                ringSource.Play();
            }
        }
    }

    private void OnHouseExit()
    {
        if (musicSource.isPlaying == false) { 
            musicSource.Play(); 
        }
        blizzardSource.volume = outdoorsVolume;
    }
}
