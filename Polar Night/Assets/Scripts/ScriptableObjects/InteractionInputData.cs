﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InteractionInputData", 
    menuName = "InteractionSystem/InputData")]
public class InteractionInputData : ScriptableObject
{
    private bool _interactedClicked;
    private bool _interactedRelease;

    public bool InteractedClicked
    {
        get => _interactedClicked;
        set => _interactedClicked = value;
    }

    public bool InteractedRelease
    {
        get => _interactedRelease;
        set => _interactedRelease = value;
    }

    public void Reset()
    {
        _interactedClicked = false;
        _interactedRelease = false;
    }
}
