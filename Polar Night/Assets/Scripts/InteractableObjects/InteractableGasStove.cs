﻿using System;
using UnityEngine;

public class InteractableGasStove : InteractableBase
{
    [SerializeField] private AudioSource addingPorridgeSource;
    [SerializeField] private AudioSource waterBoilingSource;
    [SerializeField] private AudioSource fireSorce;

    [SerializeField] private InteractionTeleportableObject waterCup;
    [SerializeField] private float waterBoilingTime = 10.0f;

    private bool _isWaterBoiled;
    private bool _isKashaTaken;

    public override void Activate()
    {
        base.Activate();

        if (_isKashaTaken && _isWaterBoiled)
        {
            TooltipHoverText = Constants.TooltipHoverBoiledWaterForKasha;
            TooltipHoldText = Constants.TooltipHoldBoiledWaterForKasha;
        }
    }

    public override void OnInteract()
    {
        if (_isWaterBoiled)
        {
            EventManager.TriggerEvent(Constants.EventKashaReady);
            gameObject.layer = 0;
            Destroy(this);
        }
        else
        {
            WaterWarmingUp();
        }
        
        base.OnInteract();
    }

    private void WaterWarmingUp()
    {
        SoundManager.singleton.PlayInteractingSound(fireSorce);

        Debug.Log("WATER WARMING UP");
        waterCup.ToInteractedSlot();
        EventManager.TriggerEvent(Constants.EventWaterWarmingUp);
        TooltipNotInteractableText = Constants.TooltipNotInteractableWarmingWater;
        EventManager.StartListening(Constants.EventKashaTaken, KashaTaken);
        Invoke(nameof(WaterBoiled), waterBoilingTime);
        Deactivate();
    }

    private void WaterBoiled()
    {
        SoundManager.singleton.PlayInteractingSound(waterBoilingSource);

        Debug.Log("WATER BOILED");
        _isWaterBoiled = true;
        TooltipNotInteractableText = Constants.TooltipNotInteractableBoiledWater;
        
        if (_isKashaTaken)
        {
            Activate();
        }
    }

    private void OnKashaCombined()
    {
        SoundManager.singleton.PlayInteractingSound(addingPorridgeSource);
        waterBoilingSource.Stop();
        fireSorce.Stop();
    }

    private void KashaTaken()
    {
        _isKashaTaken = true;

        if (_isWaterBoiled)
        {
            Activate();
        }
    }

    protected override void Start()
    {
        _isWaterBoiled = false;
        _isKashaTaken = false;
        EventManager.StartListening(Constants.EventSnowTaken, Activate);
        EventManager.StartListening(Constants.EventKashaReady, OnKashaCombined);
    }
}
