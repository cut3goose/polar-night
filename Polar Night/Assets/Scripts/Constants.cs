﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const string EmptyString = "";
    
    public const string TagPlayer = "Player";

    public const string LayerBuilding = "Building";
    public const string LayerSnow = "Snow";

    public const string TooltipHoverDefaultText = "Взаимодействовать";
    public const string TooltipHoldDefaultText = "Взаимодействую";
    public const string TooltipNotInteractableDefaultText = "Не могу взаимодействовать";
    
    public const string TooltipHoverOpen = "Открыть";
    public const string TooltipHoverClose = "Закрыть";
    
    public const string TooltipHoverBoiledWaterForKasha = "Высыпать";
    public const string TooltipHoldBoiledWaterForKasha = "Высыпаю";
    public const string TooltipNotInteractableBoiledWater = "Нужна каша";
    public const string TooltipNotInteractableWarmingWater = "Пока не вскипела";
    public const string TooltipNotInteractableNeedGarland = "Нужна гирлянда";
    public const string TooltipNotInteractableNeedBall = "Нужен ёлочный шар";

    public const string EventGameStart = "EventGameStart";
    
    public const string EventKashaTaken = "EventKashaTaken";
    public const string EventCupTaken = "EventCupTaken";
    public const string EventSnowTaken = "EventSnowTaken";
    public const string EventWaterWarmingUp = "EventWaterWarmingUp";
    public const string EventWaterBoiled = "EventWaterBoiled";
    public const string EventKashaReady = "EventKashaCombined";
    
    public const string EventBoilerTemperatureExamined = "EventBoilerTemperatureExamined";
    public const string EventBoilerRefueled = "EventBoilerRefueled";
    
    public const string EventSnowtreeBallTaken = "EventSnowtreeBallTaken";
    public const string EventSnowtreeBallPlaced = "EventSnowtreeBallPlaced";
    public const string EventOutOfSnowtreeBalls = "EventOutOfSnowtreeBalls";
    public const string EventGarlandTaken = "EventGarlandTaken";
    public const string EventGarlandPlaced = "EventGarlandPlaced";
    
    public const string EventElectricityRepaired = "EventElectricityRepaired";
    
    public const string EventCalledHome = "EventCalledHome";

    public const string EventHouseEnter = "EventHouseEnter";
    public const string EventHouseExit = "EventHouseExit";

    public const string AnimationBoolDoorOpened = "IsOpened";
    public const string AnimationBoolHandRaised = "IsRaised";
    public const string AnimationBoolWalking = "IsWalking";
    public const string AnimationTriggerHandRaise = "Raise";
    public const string AnimationTriggerHandLower = "Lower";
    public const string AnimationTriggerFadeOut = "FadeOut";
    public const string AnimationTriggerFadeIn = "FadeIn";
    public const string AnimationTriggerFadeText = "FadeText";

    public const string SlotInteractionResultStart = "InteractionResultStartPlace";
    public const string SlotLeftHand = "LeftHandObjectSlot";
    public const string SlotRightHand = "RightHandObjectSlot";

    public const string TaskSetWaterBoil = "Поставить воду кипятиться";
    public const string TaskCookKasha = "Приготовить кашу";

    public const string TaskCheckTemperature = "Проверить температуру в котельной";
    public const string TaskAddCoal = "Подкинуть угля в печку";

    public const string TaskDecorateChristmasTree = "Украсить ёлку";

    public const string TaskRepairElectricity = "Починить свет на станции";

    public const string TaskCallFamily = "Позвонить семье";
}
