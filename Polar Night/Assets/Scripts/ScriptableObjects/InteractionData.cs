﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InteractionData", 
    menuName = "InteractionSystem/InteractionData")]
public class InteractionData : ScriptableObject
{
    private InteractableBase _interactable;

    public InteractableBase Interactable
    {
        get => _interactable;
        set => _interactable = value;
    }

    public void Interact()
    {
        _interactable.OnInteract();
        ResetData();
    }

    public bool IsSameInteractable(InteractableBase _newInteractable)
    {
        return _interactable == _newInteractable;
    }

    public bool IsEmpty() => _interactable == null;
    public void ResetData() => _interactable = null;
}
