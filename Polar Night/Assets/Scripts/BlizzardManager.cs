﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlizzardManager : MonoBehaviour
{
    [SerializeField] private ParticleSystem snowing;
    [SerializeField] private ParticleSystem blizzard;

    private void Start()
    {
        EventManager.StartListening(Constants.EventGarlandPlaced, OnBlizzardStart);
        EventManager.StartListening(Constants.EventCalledHome, OnBlizzardEnd);
    }

    //private void OnHouseExit()
    //{
    //    snowing.Play();
    //    blizzard.Play();
    //}

    //private void OnHouseEnter()
    //{
    //    snowing.Stop();
    //    blizzard.Stop();
    //}

    private void OnBlizzardStart() 
    {
        snowing.Stop();
        blizzard.Play();
    }

    private void OnBlizzardEnd()
    {
        blizzard.Stop();
        snowing.Play();
    }
}
