﻿using DG.Tweening;
using UnityEngine;

public class InteractableDoor : InteractableBase
{
    private Animator _animator;
    private bool _isOpened;
    private Tween _tween;

    public override void OnInteract()
    {
        base.OnInteract();
        
        if (_isOpened)
        {
            Close();
        }
        else
        {
            Open();
        }

        _isOpened = !_isOpened;
    }

    protected override void Start()
    {
        base.Start();
        _animator = GetComponentInChildren<Animator>();
        _isOpened = false;
    }
    
    private void Open()
    {
        if (_tween != null && _tween.IsPlaying())
        {
            return;
        }
        Debug.Log("DOOR OPENED");
        var defaultRotation = transform.eulerAngles;
        _tween = DOTween.To(() => transform.eulerAngles,
            (x) => transform.eulerAngles = x,
            defaultRotation + new Vector3(0, -90.0f, 0), 1.0f);
        _tween.OnComplete(() => _isOpened = true);
        TooltipHoverText = Constants.TooltipHoverClose;
    }

    private void Close()
    {
        if (_tween != null && _tween.IsPlaying())
        {
            return;
        }
        Debug.Log("DOOR CLOSED");
        var defaultRotation = transform.eulerAngles;
        _tween = DOTween.To(() => transform.eulerAngles,
            (x) => transform.eulerAngles = x,
            defaultRotation + new Vector3(0, 90.0f, 0), 1.0f);
        _tween.OnComplete(() => _isOpened = false);
        TooltipHoverText = Constants.TooltipHoverOpen;
    }
}
