﻿using System;
using UnityEngine;

public class InteractableSnowtree : InteractableBase, ITeleportable
{
    [SerializeField] private InteractionTeleportableObject[] snowtreeBalls;
    [SerializeField] private MeshRenderer snowtreeUsual;
    [SerializeField] private MeshRenderer snowtreeSuper;

    private MeshRenderer _meshRenderer;

    private static int _counter;
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log(_counter);
        if (_counter < snowtreeBalls.Length)
        {
            Debug.Log("BALL PLACED");
            EventManager.TriggerEvent(Constants.EventSnowtreeBallPlaced);
            if (_counter == snowtreeBalls.Length - 1)
            {
                EventManager.TriggerEvent(Constants.EventOutOfSnowtreeBalls);
                Deactivate(Constants.TooltipNotInteractableNeedGarland);
            }
            else
            {
                Deactivate(Constants.TooltipNotInteractableNeedBall);
                Memory.LastInteractedType = GetType();
            }
        }
        else
        {
            Debug.Log("GARLAND PLACED, LIGHTS ARE OUT :(");
            ActivateBeautifulTree();
            EventManager.TriggerEvent(Constants.EventGarlandPlaced);
            Deactivate(Constants.EmptyString);
        }

        ++_counter;
    }

    private void ActivateBeautifulTree()
    {
        snowtreeUsual.enabled = false;
        snowtreeSuper.enabled = true;
    }

    private void ActivateLighting()
    {
        
    }

    private void NeedBall()
    {
        Deactivate(Constants.TooltipNotInteractableNeedBall);
    }

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        EventManager.StartListening(Constants.EventBoilerRefueled, NeedBall);
        EventManager.StartListening(Constants.EventSnowtreeBallTaken, Activate);
        EventManager.StartListening(Constants.EventGarlandTaken, Activate);
        EventManager.StartListening(Constants.EventElectricityRepaired, ActivateLighting);
    }
}
