﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionInputController : MonoBehaviour
{
    [SerializeField] private InteractionInputData interactionInputData;
    
    // Start is called before the first frame update
    void Start()
    {
        interactionInputData.Reset();
    }

    // Update is called once per frame
    void Update()
    {
        GetInteractionInputData();
    }

    private void GetInteractionInputData()
    {
        interactionInputData.InteractedClicked = Input.GetButtonDown("Interact");
        interactionInputData.InteractedRelease = Input.GetButtonUp("Interact");
    }
}
