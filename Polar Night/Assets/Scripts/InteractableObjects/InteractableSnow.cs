﻿using System;
using UnityEngine;

public class InteractableSnow : InteractableBase
{
    [SerializeField] private InteractionTeleportableObject cupWithSnow;
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("SNOW TAKEN");
        Memory.LastInteractedVisual = cupWithSnow;
        Memory.LastInteractedType = GetType();
        EventManager.TriggerEvent(Constants.EventSnowTaken);
        gameObject.layer = 0;
        Destroy(this);
    }

    protected override void Start()
    {
        base.Start();
        EventManager.StartListening(Constants.EventCupTaken, Activate);
    }
}
