﻿using System;
using UnityEngine;

public class InteractableKasha : InteractableBase, ITeleportable
{
    [SerializeField] private InteractionTeleportableObject kashaBox;
    
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("KASHA TAKEN");
        Memory.LastInteractedVisual = kashaBox;
        EventManager.TriggerEvent(Constants.EventKashaTaken);
        Destroy(gameObject);
    }

    protected override void Start()
    {
        EventManager.StartListening(Constants.EventWaterWarmingUp, Activate);
    }
}
