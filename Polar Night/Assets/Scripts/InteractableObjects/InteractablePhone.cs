﻿using System;
using UnityEngine;

public class InteractablePhone : InteractableBase
{
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("GAME OVER. HAVE A NICE WINTER HOLIDAYS!");
        Memory.LastInteractedType = GetType();
        EventManager.TriggerEvent(Constants.EventCalledHome);
        Deactivate(Constants.EmptyString);
    }

    private void Ring()
    {
        Activate();
    }

    private void Start()
    {
        EventManager.StartListening(Constants.EventElectricityRepaired, Ring);
    }
}
